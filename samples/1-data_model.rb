class Card < ActiveRecord::Base
  extend Forwardable

  delegate [:a?, :b?, :c?] => :workout

  belongs_to :workout
  belongs_to :grid
  acts_as_list scope: :grid
  after_update :bust_grid_cache

  scope :complete, -> { where(complete: true) }
  scope :not_complete, -> { where(complete: false) }
  scope :running_points_total, -> { select("date, sum(points_earned) OVER (ORDER BY date)") }

  def swap_workout(workout_id)
    SwapWorkoutJob.perform_later(self, workout_id)
  end

  def user
    grid.user
  end

  def points
    points_calculator.points
  end

  def points_calculator
    @points_calculator ||= PointsCalculator.new(self)
  end

  def mark_complete
    MarkCompleteJob.perform_later(self)
  end

  def complete_status
    complete ? "complete" : "not_complete"
  end

  def bust_grid_cache
    grid.touch
  end
end

### Spec

require 'rails_helper'

RSpec.describe Card, :type => :model do
  let(:user){ create(:user) }
  let(:grid){ create(:grid, user: user) }
  let(:workout){ create(:workout) }
  let(:card){ create(:card, grid: grid, workout: workout) }

  describe "relationships" do

    it 'user has many cards through grid' do
      expect(user.cards).to include card
    end

    it 'grid has many cards' do
      expect(grid.cards).to include card
    end

    it 'card belongs to grid' do
      expect(card.grid).to eq grid
    end

    it 'card belongs to workout' do
      expect(card.workout).to eq workout
    end

    it 'a workout has many cards' do
      expect(workout.cards).to include(card)
    end
  end

  describe "points" do
    it 'should not be nil' do
      expect(card.points).to_not be_nil
    end
  end

  describe "mark complete", job: true do
    it 'marks the card as complete' do
      card.mark_complete
      expect(card.reload).to be_complete
    end

    it 'updates the level of the grid' do
      grid.update(current_level: 0)
      card.mark_complete
      expect(grid.reload.current_level).to eq 1
    end

    it 'updates the score to date' do
      card.mark_complete
      expect(card.reload.score_to_date).to_not eq 0
    end
  end
end

class CancelUnpaidArbSubscriptions
  attr_reader :shipment

  def initialize(shipment)
    @shipment = shipment
  end

  def process
    shipment.parcels.arb_unpaid.each do |parcel|
      subscription = parcel.subscription
      subscription.cancel_for_billing_problem unless parcel.paid?
    end
  end
end

# Spec


require 'rails_helper'

describe CancelUnpaidArbSubscriptions do
  let!(:subscription){ create(:subscription, status: 'active', billing_type: 'arb') }

  subject { CancelUnpaidArbSubscriptions.new(subscription.current_shipment) }

  describe "which subscriptions get selected" do

    it 'gets only active subscriptions with arb type' do
      subject.process
      expect(subscription.reload).to be_cancelled
    end

    it 'it only gets active' do
      subscription.update(status: 'pending', billing_type: 'cim')
      subject.process
      expect(subscription.reload).to_not be_cancelled
    end

    it 'does not get cim' do
      subscription.update(status: 'active', billing_type: 'cim')
      subject.process
      expect(subscription.reload).to_not be_cancelled
    end

    it 'it does not cancel paid parcels' do
      expect_any_instance_of(Parcel).to receive(:paid?) { true }
      subject.process
      expect(subscription.reload).to_not be_cancelled
    end
  end

  describe ".process and what happens if it gets cancelled" do

    it 'sets the subscription status to cancelled' do
      subject.process
      expect(subscription.reload).to be_cancelled
    end

    it 'cancels the arb subscription' do
      expect_any_instance_of(Subscription).to receive(:cancel_arb_subscription_with_authorize_net)
      subject.process
    end

    it 'destroys the current parcel' do
      subject.process
      expect(subscription.current_parcel).to be_nil
    end

    it 'sends the cancelled for billing problem mailer' do
      message_delivery = instance_double(ActionMailer::MessageDelivery)
      expect(UsersMailer).to receive(:cancelled_billing_problem).with(subscription.user).and_return(message_delivery)
      expect(message_delivery).to receive(:deliver_later)
      subject.process
    end
  end
end

# Many times I like to write integrations specs much like I'd test in the browser.
# By combining several tests into one, you save on load time get more of a full user experience.

require 'spec_helper'

describe "admin creating vouchers" do
  before do
    @admin = FactoryGirl.create(:user, :role => 'admin')
    sign_in_user @admin
  end

  it 'creates and updates the voucher' do
    click_link "Voucher Packs"
    click_link "New Voucher Pack"
    fill_in "Name", :with => "Park Place Books"
    fill_in "Location", :with => "GSU"
    fill_in "Amount", :with => "19.99"
    fill_in "Number", :with => "5"
    click_on "Create Pack"
    expect(Voucher::Pack.count).to eq 1
    expect(Voucher::Code.count).to eq 5
    expect(page).to have_content "Park Place Books"
    expect(page).to have_selector('tbody tr', count: 5)
    click_on "All Vouchers"
    click_on "Park Place Books"
    click_on "Edit"
    # Increase and edit name
    fill_in "Name", :with => "Park Place Books 2"
    fill_in "Number", :with => "6"
    click_on "Update Pack"
    expect(page).to have_content "Park Place Books 2"
    expect(page).to have_selector('tbody tr', count: 6)
    # Decrease
    click_on "Edit"
    fill_in "Number", :with => "4"
    click_on "Update Pack"
    expect(page).to have_selector('tbody tr', count: 4)
    # Delete
    click_on "Delete"
    expect(page).to have_content "Deleted"
    expect(Voucher::Pack.count).to eq 0
    expect(Voucher::Code.count).to eq 0
  end
end

describe "students using vouchers" do
  let(:voucher_pack){ create(:voucher_pack, name: "Test Voucher", amount: 10.00, number: 2) }

  before(:each) do
    create_a_book_with_asset
    @voucher_code = voucher_pack.codes.first
  end

  it 'accepts voucher and deducts price and proceeds to checkout then dashboard' do
    user = FactoryGirl.create(:user)
    add_book_to_cart @book
    visit store_cart_path
    fill_in "Code", :with => "invalid code"
    click_on "Apply Code"
    expect(page).to have_content "Sorry that code is invalid"
    fill_in "Code", :with => @voucher_code.code
    click_on "Apply Code"
    expect(page).to have_content "Voucher applied!"
    expect(page).to have_content ( @book.price - voucher_pack.amount )
    click_link "Checkout"
    page.should have_content("Please sign in or create an account")
    fill_in "Email", :with => user.email
    fill_in "Password", :with => user.password
    click_button "Sign in"
    expect(page).to have_css("h1", text: "My Account")
    expect(page).to have_content("Checkout Successful")
    expect(page).to have_content(@book.title)
    expect(@voucher_code.reload).to be_used
  end
end

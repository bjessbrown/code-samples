class PointsCalculator
  attr_reader :card, :letter

  def initialize(card)
    @card = card
    @letter = card.workout.letter
  end

  def points
    send("#{letter.downcase}_points")
  end

  def a_points
    if nth_card_in_week < 2
      card.workout.base_points
    else
      7
    end
  end

  def b_points
    if nth_card_in_week < 2
      5
    else
      0
    end
  end

  def c_points
    if nth_card_in_week < 1
      4
    else
      0
    end
  end

  def current_week
    ( card.position - 1 ) / 7 + 1
  end

  def like_cards_in_week
    alike_cards_in_week.size
  end

  def nth_card_in_week
    alike_cards_in_week.index(card)
  end

  def alike_cards_in_week
    card.grid.cards_in_week(current_week).select { |c| c.workout.letter == letter }
  end
end


### Spec


require 'rails_helper'

describe "points calculator" do
  let(:a_workout){ create(:a_workout, base_points: 8) }
  let(:b_workout){ create(:b_workout) }
  let(:c_workout){ create(:c_workout) }
  let(:card){ create(:card, date: Date.today, workout: a_workout) }

  subject { PointsCalculator.new(card) }

  describe ".current_week" do
    it 'returns the week the card falls on in the grid' do
      expect( subject.current_week ).to eq 1
    end

    it 'returns 6 in the 6th week' do
      card.position = 40
      expect( subject.current_week ).to eq 6
    end
  end

  describe "a_points" do
    it 'returns base points 2 or less in a week' do
      expect( subject.a_points ).to eq a_workout.base_points
    end

    it 'returns 7 if more than 2 in week' do
      grid = create(:grid)
      create_list(:card, 3, workout: a_workout, grid: grid)
      expect( grid.cards[0].points_calculator.a_points ).to eq 8
      expect( grid.cards[1].points_calculator.a_points ).to eq 8
      expect( grid.cards[2].points_calculator.a_points ).to eq 7
    end
  end

  describe "b_points" do
    it 'always returns 5' do
      expect( subject.b_points ).to eq 5
    end

    it 'returns 0 if after the 2nd B card in the week' do
      grid = create(:grid)
      create_list(:card, 3, workout: b_workout, grid: grid)
      expect( grid.cards[0].points_calculator.b_points ).to eq 5
      expect( grid.cards[1].points_calculator.b_points ).to eq 5
      expect( grid.cards[2].points_calculator.b_points ).to eq 0
    end
  end

  describe "c_points" do
    it 'returns 4 if 1st C card' do
      expect( subject.c_points ).to eq 4
    end

    it 'returns 0 if after the 1st C card in the week' do
      grid = create(:grid)
      create_list(:card, 3, workout: c_workout, grid: grid)
      expect( grid.cards[0].points_calculator.c_points ).to eq 4
      expect( grid.cards[1].points_calculator.c_points ).to eq 0
      expect( grid.cards[2].points_calculator.c_points ).to eq 0
    end
  end

  describe ".points" do
    let(:card){ create(:card, date: Date.today, workout: b_workout) }

    it 'calls the correct points method' do
      expect(PointsCalculator.new(card).points).to eq 5
    end
  end
end

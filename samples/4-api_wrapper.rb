# API:
#  Small API wrapper for the Sears api.
#
#  Usage:
#  @search = Sears::Product.search(params[:query])
#  @products = @search.products


class Sears::Search

  def initialize(query)
    @query = query
  end

  def query
    URI.encode(@query)
  end

  def main_url
    "http://api.developer.sears.com/v2.1"
  end

  def search
    @search ||= JSON.parse(RestClient.get main_url + "/products/search/Sears/json/keyword/#{query}?apikey=#{ENV['SEARS_API_KEY']}")
  end

  def products
    p = []
    search["SearchResults"]["Products"].each do |product|
      p << Sears::Product.new(product)
    end
    p
  end

  def count
    search["SearchResults"]["ProductCount"].to_f
  end
end


class Sears::Product
  attr_accessor :product

  def initialize(product)
    @product = product
  end

  def self.search(query)
    Sears::Search.new(query)
  end

  def name
    product["Description"]["Name"]
  end

  def image_url
    product["Description"]["ImageURL"]
  end

  def id
    product["Id"]["PartNumber"]
  end

  def price
    product["Price"]["DisplayPrice"].to_f
  end
end


# Search Spec


require 'rails_helper'

describe Sears::Search do

  describe "#search" do
    subject { Sears::Search.new("shirt").search }

    it 'returns the json hash' do
      expect(subject.class).to eq Hash
    end
  end

  describe "#product" do
    subject { Sears::Search.new("red shirt").products }

    it 'returns an array' do
      expect(subject.class).to eq Array
    end

    it 'returns instances of Sears::Products' do
      expect(subject.first.class).to eq Sears::Product
    end
  end

  describe "#count" do
    let(:json){
      {
        "SearchResults" => {
          "ProductCount" => "25"
        }
      }
    }
    subject { Sears::Search.new("red shirt") }

    before { allow(subject).to receive(:search){ json }}

    it 'returns a count oof search results' do
      expect(subject.count).to eq 25
    end

    it 'returns an float' do
      expect(subject.count.class).to eq Float
    end
  end
end


# Product Spec

require 'rails_helper'

describe Sears::Product do

  describe ".search" do
    subject { Sears::Product.search("name") }

    it 'returns an the search object' do
      expect(subject.class).to eq Sears::Search
    end
  end

  describe "#name" do
    let(:product) {
      {
        "Description" => {
          "Name" => "Prod Name"
        }
      }
    }

    subject { Sears::Product.new(product) }

    it 'grabs the name attribute from the payload' do
      expect(subject.name).to eq "Prod Name"
    end
  end
end
